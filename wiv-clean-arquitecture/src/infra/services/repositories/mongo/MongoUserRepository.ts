import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserRepository } from 'src/domain/application/services/UserRepository';
import { User } from 'src/domain/entities/user.entity';
import { Repository } from 'typeorm';
import { UserEntity } from './user.entity.mongo';

@Injectable()
export class MongoUserRepository implements UserRepository {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {}
  async getByEmail(email: string): Promise<User> {
    const user = await this.userRepository.findOne({
      where: { email: email },
    });
    return UserEntity.toDomain(user);
  }
  async save(user: User): Promise<void> {
    await this.userRepository.insert(UserEntity.fromDomain(user));
  }
}
