import { TypeOrmModule } from '@nestjs/typeorm';
import { mongoPwd, mongoUser } from './mongoVars';
import { TaskEntity } from './task.engity.mongo';
import { UserEntity } from './user.entity.mongo';

export function createTypeOrmForRoot() {
  return TypeOrmModule.forRoot({
    type: 'mongodb',
    url: `mongodb+srv://${mongoUser}:${mongoPwd}@cluster0.kv2qy.mongodb.net/`,
    database: 'nest-test',
    useUnifiedTopology: true,
    entities: [UserEntity, TaskEntity],
    ssl: true,
    autoLoadEntities: true,
  });
}

export function createTypeOrmUserEntity() {
  return TypeOrmModule.forFeature([UserEntity, TaskEntity]);
}
