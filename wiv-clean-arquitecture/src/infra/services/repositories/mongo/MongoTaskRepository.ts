import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TaskRepository } from 'src/domain/application/services/TaskRepository';
import { Task } from 'src/domain/entities/task.entity';
import { Repository } from 'typeorm';
import { TaskEntity } from './task.engity.mongo';

@Injectable()
export class MongoTaskRepository implements TaskRepository {
  constructor(
    @InjectRepository(TaskEntity)
    private readonly taskRepository: Repository<TaskEntity>,
  ) {}
  async delete(task: Task): Promise<void> {
    const entity = TaskEntity.fromDomain(task);
    await this.taskRepository.remove(entity);
  }
  async listFilterByUserId(userId: string): Promise<Task[]> {
    const tasks = await this.taskRepository.find({ where: { userId: userId } });
    return tasks.map((task) => TaskEntity.toDomain(task));
  }
  async update(task: Task): Promise<void> {
    const entity = TaskEntity.fromDomain(task);
    await this.taskRepository.save(entity);
  }
  async getById(id: string): Promise<Task> {
    const entity = await this.taskRepository.findOne({ where: { _id: id } });
    return TaskEntity.toDomain(entity);
  }
  async save(task: Task): Promise<void> {
    await this.taskRepository.insert(TaskEntity.fromDomain(task));
  }
}
