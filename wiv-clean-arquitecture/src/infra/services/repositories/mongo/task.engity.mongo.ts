import { Task } from 'src/domain/entities/task.entity';
import { Column, Entity, ObjectIdColumn } from 'typeorm';

@Entity('tasks2')
export class TaskEntity {
  @ObjectIdColumn({ type: 'string', generated: false })
  readonly _id: string;
  @Column() content: string;
  @Column() done: boolean;
  @Column() userId: string;

  private constructor(props: Omit<TaskEntity, 'toDomain'>) {
    if (props !== undefined) {
      this._id = props._id;
      this.content = props.content;
      this.done = props.done;
      this.userId = props.userId;
    }
  }
  static toDomain(entity: TaskEntity): Task {
    if (entity === null || entity === undefined) return null;
    return new Task(entity._id, entity.content, entity.done, entity.userId);
  }
  static fromDomain(user: Task): TaskEntity {
    const { id, ...props } = user;
    return new TaskEntity({ _id: id, ...props });
  }
}
