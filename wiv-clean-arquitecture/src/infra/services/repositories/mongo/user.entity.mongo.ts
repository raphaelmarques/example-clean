import { User } from 'src/domain/entities/user.entity';
import { Column, Entity, ObjectIdColumn } from 'typeorm';

@Entity('users2')
export class UserEntity {
  @ObjectIdColumn({ type: 'string', generated: false })
  readonly _id: string;
  @Column() email: string;
  @Column() password: string;

  private constructor(props: Omit<UserEntity, 'toDomain'>) {
    if (props !== undefined) {
      this._id = props._id;
      this.email = props.email;
      this.password = props.password;
    }
  }

  static toDomain(entity: UserEntity): User {
    if (entity === null || entity === undefined) return null;
    return new User(entity._id, entity.email, entity.password);
  }
  static fromDomain(user: User): UserEntity {
    const { id, ...props } = user;
    return new UserEntity({ _id: id, ...props });
  }
}
