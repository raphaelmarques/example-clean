import { EmailService } from 'src/domain/application/services/EmailService';

export class MockEmailService implements EmailService {
  sentEmails: { email: string; name: 'Welcome' }[] = [];
  async sendWelcome(email: string): Promise<void> {
    this.sentEmails.push({ email: email, name: 'Welcome' });
  }
}
