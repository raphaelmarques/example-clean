import { Task } from 'src/domain/entities/task.entity';
import { TaskRepository } from 'src/domain/application/services/TaskRepository';

export class MemoryTaskRepository implements TaskRepository {
  tasks: Task[] = [];
  async getById(id: string): Promise<Task> {
    return this.tasks.find((e) => e.id === id);
  }
  async save(Task: Task): Promise<void> {
    this.tasks.push(Task);
  }
  async update(task: Task): Promise<void> {
    const index = this.tasks.findIndex((e) => e.id === task.id);
    this.tasks[index] = task;
  }
  async listFilterByUserId(userId: string): Promise<Task[]> {
    return this.tasks.filter((e) => e.userId === userId);
  }
  async delete(task: Task): Promise<void> {
    this.tasks = this.tasks.filter((e) => e.id !== task.id);
  }
}
