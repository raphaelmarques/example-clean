import { UserRepository } from 'src/domain/application/services/UserRepository';
import { User } from 'src/domain/entities/user.entity';

export class MemoryUserRepository implements UserRepository {
  users: User[] = [];
  async getByEmail(email: string): Promise<User> {
    return this.users.find((e) => e.email === email);
  }
  async save(user: User): Promise<void> {
    this.users.push(user);
  }
}
