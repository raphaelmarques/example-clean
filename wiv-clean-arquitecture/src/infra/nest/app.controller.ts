import {
  Body,
  Controller,
  Delete,
  Get,
  Param,
  Post,
  Put,
  Req,
} from '@nestjs/common';
import { Request } from 'express';
import { Task } from 'src/domain/entities/task.entity';
import { TaskAdapter } from '../adapters/taskAdapter';
import { UserAdapter, UserDto } from '../adapters/userAdapter';
import { AppService } from './app.service';

@Controller()
export class AppController {
  constructor(
    private readonly userAdapter: UserAdapter,
    private readonly taskAdapter: TaskAdapter,
    private readonly appService: AppService,
  ) {}

  @Get()
  getOk(): string {
    return 'ok';
  }

  @Post('users')
  async signup(
    @Body('email') email: string,
    @Body('password') password: string,
  ): Promise<UserDto> {
    return this.userAdapter.signup({ email, password });
  }

  @Post('tasks')
  async createTask(
    @Body('content') content: string,
    @Req() req: Request,
  ): Promise<Task> {
    const userLogin = await this.appService.basicAuthenticate(
      req.headers.authorization,
    );
    return this.taskAdapter.createTask(userLogin, content);
  }

  @Get('tasks')
  async listTasksFromUser(@Req() req: Request): Promise<Task[]> {
    const userLogin = await this.appService.basicAuthenticate(
      req.headers.authorization,
    );
    return this.taskAdapter.listTasksFromUser(userLogin);
  }

  @Put('tasks/:id/content')
  async udpateTaskContent(
    @Param('id') taskId: string,
    @Body('content') content: string,
    @Req() req: Request,
  ): Promise<Task> {
    const userLogin = await this.appService.basicAuthenticate(
      req.headers.authorization,
    );
    return this.taskAdapter.updateTaskContent(userLogin, taskId, content);
  }

  @Put('tasks/:id/done')
  async udpateTaskDone(
    @Param('id') taskId: string,
    @Body('done') done: boolean,
    @Req() req: Request,
  ): Promise<Task> {
    const userLogin = await this.appService.basicAuthenticate(
      req.headers.authorization,
    );
    return this.taskAdapter.updateTaskDone(userLogin, taskId, done);
  }

  @Delete('tasks/:id')
  async deleteTask(
    @Param('id') taskId: string,
    @Req() req: Request,
  ): Promise<void> {
    const userLogin = await this.appService.basicAuthenticate(
      req.headers.authorization,
    );
    return this.taskAdapter.deleteTask(userLogin, taskId);
  }
}
