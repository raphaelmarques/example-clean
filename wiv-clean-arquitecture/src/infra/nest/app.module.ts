import { Module } from '@nestjs/common';
import { EMAIL_SERVICE_NAME } from 'src/domain/application/services/EmailService';
import { TASK_REPOSITORY_NAME } from 'src/domain/application/services/TaskRepository';
import { USER_REPOSITORY_NAME } from 'src/domain/application/services/UserRepository';
import { TaskAdapter } from '../adapters/taskAdapter';
import { UserAdapter } from '../adapters/userAdapter';
import { MockEmailService } from '../services/repositories/email/MockEmailService';
import { MemoryTaskRepository } from '../services/repositories/memory/MemoryTaskRepository';
import { MemoryUserRepository } from '../services/repositories/memory/MemoryUserRepository';
import {
  createTypeOrmForRoot,
  createTypeOrmUserEntity,
} from '../services/repositories/mongo/configMongo';
import { MongoTaskRepository } from '../services/repositories/mongo/MongoTaskRepository';
import { MongoUserRepository } from '../services/repositories/mongo/MongoUserRepository';
import { AppController } from './app.controller';
import { AppService } from './app.service';

@Module({
  // imports: [createTypeOrmForRoot(), createTypeOrmUserEntity()],
  controllers: [AppController],
  providers: [
    { provide: USER_REPOSITORY_NAME, useClass: MemoryUserRepository },
    { provide: TASK_REPOSITORY_NAME, useClass: MemoryTaskRepository },
    // { provide: USER_REPOSITORY_NAME, useClass: MongoUserRepository },
    // { provide: TASK_REPOSITORY_NAME, useClass: MongoTaskRepository },
    { provide: EMAIL_SERVICE_NAME, useClass: MockEmailService },
    UserAdapter,
    TaskAdapter,
    AppService,
  ],
})
export class AppModule {}
