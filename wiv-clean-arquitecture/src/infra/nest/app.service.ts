import { Injectable, UnauthorizedException } from '@nestjs/common';
import { UserAdapter, UserDto } from '../adapters/userAdapter';

@Injectable()
export class AppService {
  constructor(private readonly userAdapter: UserAdapter) {}

  async basicAuthenticate(basicAuth: string): Promise<UserDto> {
    if (basicAuth == null) throw new UnauthorizedException();
    try {
      const auth = this.decode(basicAuth.split(' ')[1]);
      const [email, password] = auth.split(':');
      return this.userAdapter.login({ email, password });
    } catch (error) {
      throw new UnauthorizedException();
    }
  }
  private decode(b64: string) {
    // eslint-disable-next-line @typescript-eslint/no-var-requires
    const atob = require('atob');
    return atob(b64);
  }
}
