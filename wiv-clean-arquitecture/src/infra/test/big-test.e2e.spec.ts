import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../nest/app.module';

interface UserAuth {
  email: string;
  password: string;
}
interface UserResult {
  id: string;
  email: string;
}
interface TaskResult {
  id: string;
  userId: string;
  content: string;
  done: boolean;
}

describe('big test!!! (e2e)', () => {
  let app: INestApplication;
  const user1Auth: UserAuth = { email: 'a@abc.com', password: '123' };
  const user2Auth: UserAuth = { email: 'b@abc.com', password: '1234' };
  const user3Auth: UserAuth = { email: 'c@abc.com', password: '12345' };

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();
    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('big test', async () => {
    const user1 = await signup(app, user1Auth);
    const user2 = await signup(app, user2Auth);
    const user3 = await signup(app, user3Auth);

    expect(await (await listTasks(app, user1, user1Auth)).length).toBe(0);
    expect(await (await listTasks(app, user2, user2Auth)).length).toBe(0);
    expect(await (await listTasks(app, user3, user3Auth)).length).toBe(0);

    const task1 = await createTask(app, user1, user1Auth, 'aaa');
    const task2 = await createTask(app, user1, user1Auth, 'bbb');
    const task3 = await createTask(app, user1, user1Auth, 'ccc');
    const task4 = await createTask(app, user2, user2Auth, 'ddd');
    const task5 = await createTask(app, user2, user2Auth, 'eee');

    let tasksFrom1 = await listTasks(app, user1, user1Auth);
    const tasksFrom2 = await listTasks(app, user2, user2Auth);
    const tasksFrom3 = await listTasks(app, user3, user3Auth);
    expect(tasksFrom1.length).toBe(3);
    expect(tasksFrom2.length).toBe(2);
    expect(tasksFrom3.length).toBe(0);
    expect(tasksFrom1).toContainEqual(task1);
    expect(tasksFrom1).toContainEqual(task2);
    expect(tasksFrom1).toContainEqual(task3);
    expect(tasksFrom2).toContainEqual(task4);
    expect(tasksFrom2).toContainEqual(task5);

    const task1b = await updateTaskContent(app, user1Auth, task1.id, 'abc');
    const task2b = await updateTaskDone(app, user1Auth, task2.id, true);
    await deleteTask(app, user1Auth, task3.id);

    tasksFrom1 = await listTasks(app, user1, user1Auth);
    expect(tasksFrom1).toContainEqual(task1b);
    expect(tasksFrom1).toContainEqual(task2b);
    expect(tasksFrom1.length).toBe(2);
  });
});

async function signup(
  app: INestApplication,
  userAuth: UserAuth,
): Promise<UserResult> {
  const res = await request(app.getHttpServer())
    .post('/users')
    .send(userAuth)
    .expect(201);
  const user = res.body as UserResult;
  expect(user.id).toBeDefined();
  expect(user.email).toBe(userAuth.email);
  return user;
}
async function createTask(
  app: INestApplication,
  user: UserResult,
  userAuth: UserAuth,
  content: string,
) {
  const res = await request(app.getHttpServer())
    .post('/tasks')
    .auth(userAuth.email, userAuth.password, { type: 'basic' })
    .send({ content })
    .expect(201);
  const task = res.body as TaskResult;
  expect(task.id).toBeDefined();
  expect(task.userId).toBe(user.id);
  expect(task.content).toBe(content);
  expect(task.done).toBeFalsy();
  return task;
}

async function listTasks(
  app: INestApplication,
  user: UserResult,
  userAuth: UserAuth,
) {
  const res = await request(app.getHttpServer())
    .get('/tasks')
    .auth(userAuth.email, userAuth.password, { type: 'basic' })
    .expect(200);
  const tasks = res.body as TaskResult[];
  tasks.forEach((task) => {
    expect(task.userId).toBe(user.id);
  });
  return tasks;
}
async function updateTaskContent(
  app: INestApplication,
  userAuth: UserAuth,
  taskId: string,
  content: string,
) {
  const res = await request(app.getHttpServer())
    .put(`/tasks/${taskId}/content`)
    .auth(userAuth.email, userAuth.password, { type: 'basic' })
    .send({ content: content })
    .expect(200);
  const task = res.body as TaskResult;
  expect(task.id).toBe(taskId);
  expect(task.content).toBe(content);
  return task;
}
async function updateTaskDone(
  app: INestApplication,
  userAuth: UserAuth,
  taskId: string,
  done: boolean,
) {
  const res = await request(app.getHttpServer())
    .put(`/tasks/${taskId}/done`)
    .auth(userAuth.email, userAuth.password, { type: 'basic' })
    .send({ done: done })
    .expect(200);
  const task = res.body as TaskResult;
  expect(task.id).toBe(taskId);
  expect(task.done).toBe(done);
  return task;
}
async function deleteTask(
  app: INestApplication,
  userAuth: UserAuth,
  taskId: string,
) {
  await request(app.getHttpServer())
    .delete(`/tasks/${taskId}`)
    .auth(userAuth.email, userAuth.password, { type: 'basic' })
    .expect(200);
}
