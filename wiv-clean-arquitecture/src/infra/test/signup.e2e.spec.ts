import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../nest/app.module';
import { UserDto } from '../adapters/userAdapter';

describe('signup (e2e)', () => {
  let app: INestApplication;

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    app = moduleFixture.createNestApplication();
    await app.init();
  });

  it('/users (POST) - new user', async () => {
    const res = await request(app.getHttpServer())
      .post('/users')
      .send({ email: 'a@abc.com', password: '123' })
      .expect(201);
    const user: UserDto = res.body;
    expect(user.id).toBeDefined();
    expect(user.email).toBe('a@abc.com');
  });

  it('/users (POST) - already exist user', async () => {
    await request(app.getHttpServer())
      .post('/users')
      .send({ email: 'a@abc.com', password: '123' })
      .expect(201);
    await request(app.getHttpServer())
      .post('/users')
      .send({ email: 'a@abc.com', password: '123' })
      .expect(400);
  });

  it('/users (POST) - invalid email', async () => {
    return request(app.getHttpServer())
      .post('/users')
      .send({ email: 'abc.com', password: '123' })
      .expect(400);
  });
});
