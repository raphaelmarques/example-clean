import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { AppModule } from '../nest/app.module';
import { MemoryUserRepository } from '../services/repositories/memory/MemoryUserRepository';
import { USER_REPOSITORY_NAME } from 'src/domain/application/services/UserRepository';
import { User } from 'src/domain/entities/user.entity';
import { MemoryTaskRepository } from '../services/repositories/memory/MemoryTaskRepository';
import { TASK_REPOSITORY_NAME } from 'src/domain/application/services/TaskRepository';

describe('createTask (e2e)', () => {
  let app: INestApplication;
  let userRepository: MemoryUserRepository;
  let taskRepository: MemoryTaskRepository;

  const user = new User('a', 'a@abc.com', '123');

  beforeEach(async () => {
    const moduleFixture: TestingModule = await Test.createTestingModule({
      imports: [AppModule],
    }).compile();

    userRepository = moduleFixture.get<MemoryUserRepository>(
      USER_REPOSITORY_NAME,
    );
    taskRepository = moduleFixture.get<MemoryTaskRepository>(
      TASK_REPOSITORY_NAME,
    );

    userRepository.users = [user];

    app = moduleFixture.createNestApplication();

    await app.init();
  });

  it('/tasks (POST)', async () => {
    const res = await request(app.getHttpServer())
      .post('/tasks')
      .auth(user.email, user.password, { type: 'basic' })
      .send({ content: 'task' })
      .expect(201);
    const task = res.body as {
      id: string;
      userId: string;
      done: boolean;
      content: string;
    };
    expect(task).toBeDefined();
    expect(task.id).toBeDefined();
    expect(task.userId).toBe(user.id);
    expect(task.done).toBe(false);
    expect(task.content).toBe('task');
    expect(taskRepository.tasks.length).toBe(1);
    expect(taskRepository.tasks[0].id).toBe(task.id);
  });
  it('/tasks (POST) - without auth', async () => {
    return request(app.getHttpServer())
      .post('/tasks')
      .send({ content: 'task' })
      .expect(401);
  });
  it('/tasks (POST) - invalid password', async () => {
    return request(app.getHttpServer())
      .post('/tasks')
      .auth(user.email, 'x', { type: 'basic' })
      .send({ content: 'task' })
      .expect(401);
  });
  it('/tasks (POST) - invalid user', async () => {
    return request(app.getHttpServer())
      .post('/tasks')
      .auth('x', 'x', { type: 'basic' })
      .send({ content: 'task' })
      .expect(401);
  });
});
