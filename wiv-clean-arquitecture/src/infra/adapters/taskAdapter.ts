import {
  BadRequestException,
  ForbiddenException,
  Inject,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import {
  TaskRepository,
  TASK_REPOSITORY_NAME,
} from 'src/domain/application/services/TaskRepository';
import { CreateTaskUseCase } from 'src/domain/application/use-cases/task/createTask';
import { DeleteTaskUseCase } from 'src/domain/application/use-cases/task/deleteTask';
import { ListTasksFromUserUseCase } from 'src/domain/application/use-cases/task/listTasksFromUser';
import { UpdateTaskContentUseCase } from 'src/domain/application/use-cases/task/updateTaskContent';
import { UpdateTaskDoneUseCase } from 'src/domain/application/use-cases/task/updateTaskDone';
import { Task } from 'src/domain/entities/task.entity';
import { User } from 'src/domain/entities/user.entity';
import { TaskNotFoundException } from 'src/domain/exceptions/TaskNotFoundException';
import { UnauthorizedException } from 'src/domain/exceptions/UnauthorizedException';
import { UserDto } from './userAdapter';

@Injectable()
export class TaskAdapter {
  private createTaskUseCase: CreateTaskUseCase;
  private deleteTaskUseCase: DeleteTaskUseCase;
  private listTasksFromUserUseCase: ListTasksFromUserUseCase;
  private updateTaskContentUseCase: UpdateTaskContentUseCase;
  private updateTaskDoneUseCase: UpdateTaskDoneUseCase;
  constructor(
    @Inject(TASK_REPOSITORY_NAME)
    private readonly taskRepository: TaskRepository,
  ) {
    this.createTaskUseCase = new CreateTaskUseCase(taskRepository);
    this.deleteTaskUseCase = new DeleteTaskUseCase(taskRepository);
    this.listTasksFromUserUseCase = new ListTasksFromUserUseCase(
      taskRepository,
    );
    this.updateTaskContentUseCase = new UpdateTaskContentUseCase(
      taskRepository,
    );
    this.updateTaskDoneUseCase = new UpdateTaskDoneUseCase(taskRepository);
  }
  async createTask(user: UserDto, content: string): Promise<Task> {
    return this.createTaskUseCase
      .execute(user as User, content)
      .catch((reason) => this.handleCreateTaskErrors(reason));
  }
  async deleteTask(user: UserDto, taskId: string): Promise<void> {
    return this.deleteTaskUseCase
      .execute(user as User, taskId)
      .catch((reason) => this.handleDeleteTaskErrors(reason));
  }
  async listTasksFromUser(user: UserDto): Promise<Task[]> {
    return this.listTasksFromUserUseCase
      .execute(user as User)
      .catch((reason) => this.handleListTasksFromUserErrors(reason));
  }
  async updateTaskContent(
    user: UserDto,
    taskId: string,
    content: string,
  ): Promise<Task> {
    return this.updateTaskContentUseCase
      .execute(user as User, taskId, content)
      .catch((reason) => this.handleUpdateTaskContentErrors(reason));
  }
  async updateTaskDone(
    user: UserDto,
    taskId: string,
    done: boolean,
  ): Promise<Task> {
    return this.updateTaskDoneUseCase
      .execute(user as User, taskId, done)
      .catch((reason) => this.handleUpdateTaskDoneErrors(reason));
  }

  private handleCreateTaskErrors(reason: any): Task {
    return this.handleDefaultError(reason);
  }
  private handleDeleteTaskErrors(reason: any): void {
    if (reason instanceof UnauthorizedException) throw new ForbiddenException();
    if (reason instanceof TaskNotFoundException) throw new NotFoundException();
    return this.handleDefaultError(reason);
  }
  private handleListTasksFromUserErrors(reason: any): Task[] {
    return this.handleDefaultError(reason);
  }
  private handleUpdateTaskContentErrors(reason: any): Task {
    if (reason instanceof UnauthorizedException) throw new ForbiddenException();
    if (reason instanceof TaskNotFoundException) throw new NotFoundException();
    return this.handleDefaultError(reason);
  }
  private handleUpdateTaskDoneErrors(reason: any): Task {
    if (reason instanceof UnauthorizedException) throw new ForbiddenException();
    if (reason instanceof TaskNotFoundException) throw new NotFoundException();
    return this.handleDefaultError(reason);
  }
  private handleDefaultError<T>(reason: any): T {
    if (reason.message !== undefined)
      throw new BadRequestException({
        statusCode: 400,
        message: reason.message,
      });
    else throw new BadRequestException();
  }
}
