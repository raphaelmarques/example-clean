import {
  BadRequestException,
  Inject,
  Injectable,
  UnauthorizedException,
} from '@nestjs/common';
import {
  EmailService,
  EMAIL_SERVICE_NAME,
} from 'src/domain/application/services/EmailService';
import {
  UserRepository,
  USER_REPOSITORY_NAME,
} from 'src/domain/application/services/UserRepository';
import { LoginUseCase } from 'src/domain/application/use-cases/user/login';
import { SignupUseCase } from 'src/domain/application/use-cases/user/signup';
import { User } from 'src/domain/entities/user.entity';
import { InvalidPasswordException } from 'src/domain/exceptions/InvalidPasswordException';
import { UserNotFoundException } from 'src/domain/exceptions/UserNotFoundException';

@Injectable()
export class UserAdapter {
  private signupUseCase: SignupUseCase;
  private loginUseCase: LoginUseCase;
  constructor(
    @Inject(USER_REPOSITORY_NAME)
    private readonly userRepository: UserRepository,
    @Inject(EMAIL_SERVICE_NAME)
    private readonly emailService: EmailService,
  ) {
    this.signupUseCase = new SignupUseCase(userRepository, emailService);
    this.loginUseCase = new LoginUseCase(userRepository);
  }

  async signup(props: { email: string; password: string }): Promise<UserDto> {
    const user = await this.signupUseCase
      .execute(props.email, props.password)
      .catch((reason) => this.handleSignupErrors(reason));
    return convertToUserDto(user);
  }
  async login(props: { email: string; password: string }): Promise<UserDto> {
    const user = await this.loginUseCase
      .execute(props.email, props.password)
      .catch((reason) => this.handleLoginErrors(reason));
    return convertToUserDto(user);
  }

  private handleLoginErrors(reason: any): User {
    if (reason instanceof UserNotFoundException)
      throw new UnauthorizedException();
    if (reason instanceof InvalidPasswordException)
      throw new UnauthorizedException();
    throw new BadRequestException();
  }

  private handleSignupErrors(reason: any): User {
    throw new BadRequestException();
  }
}

export interface UserDto {
  id: string;
  email: string;
}
function convertToUserDto(user: User): UserDto {
  return {
    id: user.id,
    email: user.email,
  };
}
