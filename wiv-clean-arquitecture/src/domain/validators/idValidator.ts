export function validateId(id: string): boolean {
  if (id === null) return false;
  if (id === undefined) return false;
  if (typeof id !== 'string') return false;
  return id.length > 0;
}
