import { validateEmail } from './emailValidator';

describe('Email Validator', () => {
  const validEmails = [
    ['name@abc.com'],
    ['name+123@abc.com'],
    ['name.lastname.a.1.2-3-abc@abc.xyz.a.b.com'],
  ];
  it.each(validEmails)('valid email $#', (email: string) => {
    const isValid = validateEmail(email);
    expect(isValid).toBeTruthy();
  });

  const invalidEmails = [
    ['@abc.com'],
    ['name@'],
    ['name @abc.com'],
    ['name@abc'],
    ['name@abc com'],
    ['name.abc.com'],
    ['@'],
    ['name'],
    ['name@abc.com@abc.com'],
    ['a@b'],
    [''],
    [null],
    [undefined],
  ];
  it.each(invalidEmails)('invalid email $#', (email: string) => {
    const isValid = validateEmail(email);
    expect(isValid).toBeFalsy();
  });
});
