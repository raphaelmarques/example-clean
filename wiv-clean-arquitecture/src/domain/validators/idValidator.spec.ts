import { validateId } from './idValidator';

describe('idValidator', () => {
  const validIds = ['a', 'abc', '123'];
  it.each(validIds.map((e) => e))('valid id [%#] %p', (id: string) => {
    const isValid = validateId(id);
    expect(isValid).toBeTruthy();
  });

  const invalidIds = ['', null, undefined];
  it.each(invalidIds.map((e) => e))('invalid id [%#] %p', (id: string) => {
    const isValid = validateId(id);
    expect(isValid).toBeFalsy();
  });
});
