import { InvalidEmailException } from '../exceptions/InvalidEmailException';
import { InvalidIdException } from '../exceptions/InvalidIdException';
import { validateEmail } from '../validators/emailValidator';
import { validateId } from '../validators/idValidator';

export class User {
  id: string;
  email: string;
  password: string;

  constructor(id: string, email: string, password: string) {
    if (!validateId(id)) throw new InvalidIdException();
    if (!validateEmail(email)) throw new InvalidEmailException();
    this.id = id;
    this.email = email;
    this.password = password;
  }
}
