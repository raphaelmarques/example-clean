import { InvalidIdException } from 'src/domain/exceptions/InvalidIdException';
import { validateId } from '../validators/idValidator';

export class Task {
  id: string;
  content: string;
  done: boolean;
  userId: string;

  constructor(id: string, content: string, done: boolean, userId: string) {
    if (!validateId(id)) throw new InvalidIdException();
    if (!validateId(userId)) throw new InvalidIdException();
    this.id = id;
    this.content = content;
    this.done = done;
    this.userId = userId;
  }
}
