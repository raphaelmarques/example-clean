import { InvalidEmailException } from '../exceptions/InvalidEmailException';
import { InvalidIdException } from '../exceptions/InvalidIdException';
import { User } from './user.entity';

describe('user.entity', () => {
  it('create', () => {
    const user = new User('a', 'a@abc.com', '123');
    expect(user).toBeInstanceOf(User);
    expect(user.email).toBe('a@abc.com');
    expect(user.id).toBe('a');
    expect(user.password).toBe('123');
  });
  it('invalid id should throw InvalidIdException', () => {
    expect(() => {
      new User('', 'a@abc.com', '123');
    }).toThrow(InvalidIdException);
  });
  it('invalid email should throw InvalidEmailException', () => {
    expect(() => {
      new User('a', 'abc.com', '123');
    }).toThrow(InvalidEmailException);
  });
});
