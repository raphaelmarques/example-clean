import { InvalidIdException } from 'src/domain/exceptions/InvalidIdException';
import { Task } from './task.entity';

describe('task.entity', () => {
  it('constructor', () => {
    const task = new Task('a', 'abc', true, 'b');
    expect(task).toBeInstanceOf(Task);
    expect(task.id).toBe('a');
    expect(task.content).toBe('abc');
    expect(task.done).toBe(true);
    expect(task.userId).toBe('b');
  });
  it('invalid id should throw InvalidIdException', () => {
    expect(() => {
      new Task('', 'abc', true, 'b');
    }).toThrow(InvalidIdException);
  });
  it('invalid userId should throw InvalidIdException', () => {
    expect(() => {
      new Task('a', 'abc', true, '');
    }).toThrow(InvalidIdException);
  });
});
