import { User } from 'src/domain/entities/user.entity';
import { InvalidPasswordException } from 'src/domain/exceptions/InvalidPasswordException';
import { UserNotFoundException } from 'src/domain/exceptions/UserNotFoundException';
import { MemoryUserRepository } from 'src/infra/services/repositories/memory/MemoryUserRepository';
import { LoginUseCase } from './login';

describe('login', () => {
  let userRepository: MemoryUserRepository;
  let login: LoginUseCase;
  beforeEach(() => {
    userRepository = new MemoryUserRepository();
    userRepository.users = [new User('a', 'a@abc.com', '123')];
    login = new LoginUseCase(userRepository);
  });
  it('login', async () => {
    const user = await login.execute('a@abc.com', '123');
    expect(user).toBeInstanceOf(User);
    expect(user.email).toBe('a@abc.com');
  });
  it('wrong password', async () => {
    await expect(async () => {
      await login.execute('a@abc.com', 'aaa');
    }).rejects.toThrow(new InvalidPasswordException());
  });
  it('user not found', async () => {
    await expect(async () => {
      await login.execute('c@abc.com', '123');
    }).rejects.toThrow(new UserNotFoundException());
  });
});
