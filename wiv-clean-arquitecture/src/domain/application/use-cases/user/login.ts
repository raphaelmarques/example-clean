import { User } from 'src/domain/entities/user.entity';
import { InvalidParameterException } from 'src/domain/exceptions/InvalidParameterException';
import { InvalidPasswordException } from 'src/domain/exceptions/InvalidPasswordException';
import { UserNotFoundException } from 'src/domain/exceptions/UserNotFoundException';
import { UserRepository } from '../../services/UserRepository';

export class LoginUseCase {
  private userRepository: UserRepository;
  constructor(userRepository: UserRepository) {
    this.userRepository = userRepository;
  }
  async execute(email: string, password: string): Promise<User> {
    if (email == null) throw new InvalidParameterException('email');
    if (password == null) throw new InvalidParameterException('password');
    const user = await this.userRepository.getByEmail(email);
    if (!user) throw new UserNotFoundException();
    if (user.password !== password) throw new InvalidPasswordException();
    return user;
  }
}
