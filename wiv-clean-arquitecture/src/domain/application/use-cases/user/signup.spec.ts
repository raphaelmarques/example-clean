import { User } from 'src/domain/entities/user.entity';
import { UserAlreadyExistsException } from 'src/domain/exceptions/UserAlreadyExistsException';
import { MockEmailService } from 'src/infra/services/repositories/email/MockEmailService';
import { MemoryUserRepository } from 'src/infra/services/repositories/memory/MemoryUserRepository';
import { SignupUseCase } from './signup';

describe('signup', () => {
  let userRepository: MemoryUserRepository;
  let emailService: MockEmailService;
  let signup: SignupUseCase;
  beforeEach(() => {
    userRepository = new MemoryUserRepository();
    emailService = new MockEmailService();
    signup = new SignupUseCase(userRepository, emailService);
  });

  it('signup', async () => {
    const user = await signup.execute('a@abc.com', '123');
    expect(user).toBeInstanceOf(User);
    expect(user.email).toBe('a@abc.com');
    expect(user.password).toBe('123');
    expect(userRepository.users[0]).toEqual(user);
    expect(emailService.sentEmails.length).toBe(1);
    expect(emailService.sentEmails[0].email).toBe('a@abc.com');
    expect(emailService.sentEmails[0].name).toBe('Welcome');
  });
  it('if already exists throw UserAlreadyExistsException', async () => {
    await expect(async () => {
      await signup.execute('a@abc.com', '123');
      await signup.execute('a@abc.com', '1234');
    }).rejects.toThrow(new UserAlreadyExistsException());
  });
});
