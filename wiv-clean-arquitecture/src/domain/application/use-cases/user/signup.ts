import { User } from 'src/domain/entities/user.entity';
import { InvalidParameterException } from 'src/domain/exceptions/InvalidParameterException';
import { UserAlreadyExistsException } from 'src/domain/exceptions/UserAlreadyExistsException';
import { v4 } from 'uuid';
import { UserRepository } from '../../services/UserRepository';
import { EmailService } from '../../services/EmailService';

export class SignupUseCase {
  private userRepository: UserRepository;
  private emailService: EmailService;
  constructor(userRepository: UserRepository, emailService: EmailService) {
    this.userRepository = userRepository;
    this.emailService = emailService;
  }
  async execute(email: string, password: string): Promise<User> {
    if (email == null) throw new InvalidParameterException('email');
    if (password == null) throw new InvalidParameterException('password');
    const user = await this.userRepository.getByEmail(email);
    if (user) throw new UserAlreadyExistsException();
    const id = v4();
    const newUser = new User(id, email, password);
    await this.userRepository.save(newUser);
    await this.emailService.sendWelcome(newUser.email);
    return newUser;
  }
}
