import { Task } from 'src/domain/entities/task.entity';
import { User } from 'src/domain/entities/user.entity';
import { InvalidParameterException } from 'src/domain/exceptions/InvalidParameterException';
import { TaskRepository } from '../../services/TaskRepository';

export class ListTasksFromUserUseCase {
  private taskRepository: TaskRepository;
  constructor(taskRepository: TaskRepository) {
    this.taskRepository = taskRepository;
  }
  async execute(user: User): Promise<Task[]> {
    if (user == null) throw new InvalidParameterException('user');
    const tasks = await this.taskRepository.listFilterByUserId(user.id);
    return tasks;
  }
}
