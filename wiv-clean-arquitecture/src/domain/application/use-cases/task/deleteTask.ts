import { User } from 'src/domain/entities/user.entity';
import { InvalidParameterException } from 'src/domain/exceptions/InvalidParameterException';
import { TaskNotFoundException } from 'src/domain/exceptions/TaskNotFoundException';
import { UnauthorizedException } from 'src/domain/exceptions/UnauthorizedException';
import { TaskRepository } from '../../services/TaskRepository';

export class DeleteTaskUseCase {
  private taskRepository: TaskRepository;
  constructor(taskRepository: TaskRepository) {
    this.taskRepository = taskRepository;
  }
  async execute(user: User, taskId: string): Promise<void> {
    if (user == null) throw new InvalidParameterException('user');
    if (taskId == null) throw new InvalidParameterException('taskId');
    const task = await this.taskRepository.getById(taskId);
    if (task == null) throw new TaskNotFoundException();
    if (task.userId !== user.id) throw new UnauthorizedException();
    await this.taskRepository.delete(task);
  }
}
