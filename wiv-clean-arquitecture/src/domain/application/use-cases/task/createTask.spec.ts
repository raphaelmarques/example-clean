import { Task } from 'src/domain/entities/task.entity';
import { User } from 'src/domain/entities/user.entity';
import { InvalidParameterException } from 'src/domain/exceptions/InvalidParameterException';
import { MemoryTaskRepository } from 'src/infra/services/repositories/memory/MemoryTaskRepository';
import { TaskRepository } from '../../services/TaskRepository';
import { CreateTaskUseCase } from './createTask';

describe('create task', () => {
  let taskRepository: TaskRepository;
  let createTask: CreateTaskUseCase;
  beforeEach(() => {
    taskRepository = new MemoryTaskRepository();
    createTask = new CreateTaskUseCase(taskRepository);
  });
  it('create', async () => {
    const user = new User('a', 'a@abc.com', '123');
    const task = await createTask.execute(user, 'oi');
    expect(task).toBeInstanceOf(Task);
    expect(task.content).toBe('oi');
    expect(task.userId).toBe(user.id);
    expect(task.done).toBeFalsy();
  });
  it('accept empty content', async () => {
    const user = new User('a', 'a@abc.com', '123');
    const task = await createTask.execute(user, '');
    expect(task.content).toBe('');
  });
  it('invalid user', async () => {
    await expect(() => {
      return createTask.execute(null, '');
    }).rejects.toThrow(InvalidParameterException);
  });
  it('invalid content', async () => {
    await expect(() => {
      const user = new User('a', 'a@abc.com', '123');
      return createTask.execute(user, null);
    }).rejects.toThrow(InvalidParameterException);
  });
});
