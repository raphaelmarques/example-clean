import { Task } from 'src/domain/entities/task.entity';
import { User } from 'src/domain/entities/user.entity';
import { InvalidParameterException } from 'src/domain/exceptions/InvalidParameterException';
import { TaskNotFoundException } from 'src/domain/exceptions/TaskNotFoundException';
import { UnauthorizedException } from 'src/domain/exceptions/UnauthorizedException';
import { MemoryTaskRepository } from 'src/infra/services/repositories/memory/MemoryTaskRepository';
import { DeleteTaskUseCase } from './deleteTask';

describe('delete task', () => {
  let taskRepository: MemoryTaskRepository;
  let deleteTask: DeleteTaskUseCase;
  const userA = new User('a', 'a@abc.com', '123');
  const userB = new User('b', 'b@abc.com', '123');
  beforeEach(() => {
    taskRepository = new MemoryTaskRepository();
    taskRepository.tasks = [
      new Task('1', 'aaa', true, userA.id),
      new Task('2', 'bbb', true, userA.id),
      new Task('3', 'ccc', true, userA.id),
      new Task('4', 'ddd', true, userB.id),
      new Task('5', 'eee', true, userB.id),
    ];
    deleteTask = new DeleteTaskUseCase(taskRepository);
  });
  it('delete', async () => {
    await deleteTask.execute(userA, '1');
    expect(taskRepository.tasks.length).toBe(4);
    expect(await taskRepository.getById('1')).not.toBeDefined();
  });
  it('delete', async () => {
    await deleteTask.execute(userA, '1');
    expect(taskRepository.tasks.length).toBe(4);
    expect(await taskRepository.getById('1')).toBeFalsy();
  });
  it('delete from other user', async () => {
    await expect(() => {
      return deleteTask.execute(userB, '1');
    }).rejects.toThrow(UnauthorizedException);
  });
  it('invalid user', async () => {
    await expect(() => {
      return deleteTask.execute(null, '1');
    }).rejects.toThrow(InvalidParameterException);
  });
  it('invalid taskId', async () => {
    await expect(() => {
      return deleteTask.execute(userB, null);
    }).rejects.toThrow(InvalidParameterException);
  });
  it('not found task', async () => {
    await expect(() => {
      return deleteTask.execute(userB, '0');
    }).rejects.toThrow(TaskNotFoundException);
  });
});
