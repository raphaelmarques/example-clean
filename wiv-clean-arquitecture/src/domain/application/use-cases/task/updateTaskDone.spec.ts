import { Task } from 'src/domain/entities/task.entity';
import { User } from 'src/domain/entities/user.entity';
import { InvalidParameterException } from 'src/domain/exceptions/InvalidParameterException';
import { TaskNotFoundException } from 'src/domain/exceptions/TaskNotFoundException';
import { UnauthorizedException } from 'src/domain/exceptions/UnauthorizedException';
import { MemoryTaskRepository } from 'src/infra/services/repositories/memory/MemoryTaskRepository';
import { UpdateTaskDoneUseCase } from './updateTaskDone';

describe('update task done', () => {
  let taskRepository: MemoryTaskRepository;
  let updateTaskDone: UpdateTaskDoneUseCase;
  const user1 = new User('x', 'a@abc.com', '123');
  const user2 = new User('y', 'b@abc.com', '123');
  beforeEach(() => {
    taskRepository = new MemoryTaskRepository();
    taskRepository.tasks = [
      new Task('a', 'abc', false, user1.id),
      new Task('b', 'xyz', true, user1.id),
    ];
    updateTaskDone = new UpdateTaskDoneUseCase(taskRepository);
  });
  it('update to true', async () => {
    const task = await updateTaskDone.execute(user1, 'a', true);
    const savedTask = await taskRepository.getById('a');
    expect(task).toBeInstanceOf(Task);
    expect(savedTask.done).toBeTruthy();
  });
  it('update to false', async () => {
    const task = await updateTaskDone.execute(user1, 'b', false);
    const savedTask = await taskRepository.getById('b');
    expect(task).toBeInstanceOf(Task);
    expect(savedTask.done).toBeFalsy();
  });
  it('empty user', async () => {
    await expect(() => {
      return updateTaskDone.execute(null, 'a', true);
    }).rejects.toThrow(InvalidParameterException);
  });
  it('empty taskId', async () => {
    await expect(() => {
      return updateTaskDone.execute(user1, null, true);
    }).rejects.toThrow(InvalidParameterException);
  });
  it('empty content', async () => {
    await expect(() => {
      return updateTaskDone.execute(user1, 'a', null);
    }).rejects.toThrow(InvalidParameterException);
  });
  it('invalid user', async () => {
    await expect(() => {
      return updateTaskDone.execute(user2, 'a', true);
    }).rejects.toThrow(UnauthorizedException);
  });
  it('task not found', async () => {
    await expect(() => {
      return updateTaskDone.execute(user1, '0', true);
    }).rejects.toThrow(TaskNotFoundException);
  });
});
