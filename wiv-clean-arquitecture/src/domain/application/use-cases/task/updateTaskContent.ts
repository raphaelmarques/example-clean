import { Task } from 'src/domain/entities/task.entity';
import { User } from 'src/domain/entities/user.entity';
import { InvalidParameterException } from 'src/domain/exceptions/InvalidParameterException';
import { TaskNotFoundException } from 'src/domain/exceptions/TaskNotFoundException';
import { UnauthorizedException } from 'src/domain/exceptions/UnauthorizedException';
import { TaskRepository } from '../../services/TaskRepository';

export class UpdateTaskContentUseCase {
  private taskRepository: TaskRepository;
  constructor(taskRepository: TaskRepository) {
    this.taskRepository = taskRepository;
  }
  async execute(user: User, taskId: string, content: string): Promise<Task> {
    if (user == null) throw new InvalidParameterException('user');
    if (taskId == null) throw new InvalidParameterException('taskId');
    if (content == null) throw new InvalidParameterException('content');
    const task = await this.taskRepository.getById(taskId);
    if (task == null) throw new TaskNotFoundException();
    if (task.userId !== user.id) throw new UnauthorizedException();
    task.content = content;
    await this.taskRepository.update(task);
    return task;
  }
}
