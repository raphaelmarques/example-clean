import { Task } from 'src/domain/entities/task.entity';
import { User } from 'src/domain/entities/user.entity';
import { InvalidParameterException } from 'src/domain/exceptions/InvalidParameterException';
import { MemoryTaskRepository } from 'src/infra/services/repositories/memory/MemoryTaskRepository';
import { ListTasksFromUserUseCase } from './listTasksFromUser';

describe('list tasks from user', () => {
  let taskRepository: MemoryTaskRepository;
  let listTasksFromUser: ListTasksFromUserUseCase;
  const userA = new User('a', 'a@abc.com', '123');
  const userB = new User('b', 'b@abc.com', '123');
  const userC = new User('c', 'c@abc.com', '123');
  beforeEach(() => {
    taskRepository = new MemoryTaskRepository();
    taskRepository.tasks = [
      new Task('1', 'aaa', true, userA.id),
      new Task('2', 'bbb', true, userA.id),
      new Task('3', 'ccc', true, userA.id),
      new Task('4', 'ddd', true, userB.id),
      new Task('5', 'eee', true, userB.id),
    ];
    listTasksFromUser = new ListTasksFromUserUseCase(taskRepository);
  });
  it('list from user', async () => {
    const tasksA = await listTasksFromUser.execute(userA);
    expect(tasksA.length).toBe(3);
    expect(tasksA[0]).toEqual(taskRepository.tasks[0]);
    expect(tasksA[1]).toEqual(taskRepository.tasks[1]);
    expect(tasksA[2]).toEqual(taskRepository.tasks[2]);
    const tasksB = await listTasksFromUser.execute(userB);
    expect(tasksB.length).toBe(2);
    expect(tasksB[0]).toEqual(taskRepository.tasks[3]);
    expect(tasksB[1]).toEqual(taskRepository.tasks[4]);
  });
  it('list empty', async () => {
    const tasks = await listTasksFromUser.execute(userC);
    expect(tasks.length).toBe(0);
  });
  it('empty user', async () => {
    await expect(() => {
      return listTasksFromUser.execute(null);
    }).rejects.toThrow(InvalidParameterException);
  });
});
