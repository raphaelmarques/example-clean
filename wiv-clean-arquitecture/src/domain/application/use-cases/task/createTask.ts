import { Task } from 'src/domain/entities/task.entity';
import { User } from 'src/domain/entities/user.entity';
import { InvalidParameterException } from 'src/domain/exceptions/InvalidParameterException';
import { v4 } from 'uuid';
import { TaskRepository } from '../../services/TaskRepository';

export class CreateTaskUseCase {
  private taskRepository: TaskRepository;
  constructor(taskRepository: TaskRepository) {
    this.taskRepository = taskRepository;
  }
  async execute(user: User, content: string): Promise<Task> {
    if (user === null || user === undefined)
      throw new InvalidParameterException('user');
    if (content === null || content === undefined)
      throw new InvalidParameterException('content');
    const id = v4();
    const task = new Task(id, content, false, user.id);
    await this.taskRepository.save(task);
    return task;
  }
}
