import { Task } from 'src/domain/entities/task.entity';
import { User } from 'src/domain/entities/user.entity';
import { InvalidParameterException } from 'src/domain/exceptions/InvalidParameterException';
import { TaskNotFoundException } from 'src/domain/exceptions/TaskNotFoundException';
import { UnauthorizedException } from 'src/domain/exceptions/UnauthorizedException';
import { MemoryTaskRepository } from 'src/infra/services/repositories/memory/MemoryTaskRepository';
import { UpdateTaskContentUseCase } from './updateTaskContent';

describe('update task content', () => {
  let taskRepository: MemoryTaskRepository;
  let updateTaskContent: UpdateTaskContentUseCase;
  const user1 = new User('x', 'a@abc.com', '123');
  const user2 = new User('y', 'b@abc.com', '123');
  beforeEach(() => {
    taskRepository = new MemoryTaskRepository();
    taskRepository.tasks = [
      new Task('a', 'abc', false, user1.id),
      new Task('b', 'xyz', true, user1.id),
    ];
    updateTaskContent = new UpdateTaskContentUseCase(taskRepository);
  });
  it('update', async () => {
    const task = await updateTaskContent.execute(user1, 'a', 'hey');
    const savedTask = await taskRepository.getById('a');
    expect(task).toBeInstanceOf(Task);
    expect(task.content).toBe('hey');
    expect(savedTask.content).toBe('hey');
  });
  it('empty user', async () => {
    await expect(() => {
      return updateTaskContent.execute(null, 'a', 'hey');
    }).rejects.toThrow(InvalidParameterException);
  });
  it('empty taskId', async () => {
    await expect(() => {
      return updateTaskContent.execute(user1, null, 'hey');
    }).rejects.toThrow(InvalidParameterException);
  });
  it('empty content', async () => {
    await expect(() => {
      return updateTaskContent.execute(user1, 'a', null);
    }).rejects.toThrow(InvalidParameterException);
  });
  it('invalid user', async () => {
    await expect(() => {
      return updateTaskContent.execute(user2, 'a', 'hey');
    }).rejects.toThrow(UnauthorizedException);
  });
  it('task not found', async () => {
    await expect(() => {
      return updateTaskContent.execute(user1, '0', 'hey');
    }).rejects.toThrow(TaskNotFoundException);
  });
});
