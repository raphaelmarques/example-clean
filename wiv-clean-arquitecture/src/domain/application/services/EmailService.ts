export const EMAIL_SERVICE_NAME = 'EmailService';

export interface EmailService {
  sendWelcome(email: string): Promise<void>;
}
