import { User } from 'src/domain/entities/user.entity';

export const USER_REPOSITORY_NAME = 'UserRepository';

export interface UserRepository {
  getByEmail(email: string): Promise<User>;
  save(user: User): Promise<void>;
}
