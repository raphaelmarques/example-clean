import { Task } from 'src/domain/entities/task.entity';

export const TASK_REPOSITORY_NAME = 'TaskRepository';

export interface TaskRepository {
  delete(task: Task): Promise<void>;
  listFilterByUserId(userId: string): Promise<Task[]>;
  update(task: Task): Promise<void>;
  getById(id: string): Promise<Task>;
  save(task: Task): Promise<void>;
}
