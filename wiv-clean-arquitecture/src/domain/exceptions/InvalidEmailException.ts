export class InvalidEmailException extends Error {
  constructor() {
    super('Invalid email');
  }
}
