export class InvalidParameterException extends Error {
  constructor(parameter = '') {
    super(`Invalid parameter ${parameter}`);
  }
}
