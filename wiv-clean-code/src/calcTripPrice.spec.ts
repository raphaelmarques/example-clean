import { InvalidDateException } from "./InvalidDateException";
import { Trip } from "./trip";

describe("trip2", () => {
  it("Normal", () => {
    const trip = new Trip(new Date("2018-10-10T10:10"), 1000);
    const price = trip.calcPrice();
    expect(price).toBe(2100);
  });
  it("Sunday", () => {
    const trip = new Trip(new Date("2021-05-09T10:10"), 1000);
    const price = trip.calcPrice();
    expect(price).toBe(3000);
  });
  it("Overnight", () => {
    const trip = new Trip(new Date("2021-05-08T23:00"), 1000);
    const price = trip.calcPrice();
    expect(price).toBe(3900);
  });
  it("Invalid date", () => {
    expect(() => {
      new Trip((undefined as any) as Date, 1000);
    }).toThrow(new InvalidDateException());
  });
});
