import { InvalidDateException } from "./InvalidDateException";

export class Trip {
  private readonly OVERNIGHT_PRICE = 3.9;
  private readonly SUNDAY_PRICE = 3;
  private readonly NORMAL_PRICE = 2.1;
  private readonly OVERNIGHT_START_HOUR = 22;
  private readonly OVERNIGHT_END_HOUR = 6;

  private date: Date;
  private distance: number;
  constructor(date: Date, distance: number) {
    if (!(date instanceof Date)) throw new InvalidDateException();
    this.date = date;
    this.distance = distance;
  }

  calcPrice(): number {
    if (this.isOvernight()) {
      return this.distance * this.OVERNIGHT_PRICE;
    }
    if (this.isSunday()) {
      return this.distance * this.SUNDAY_PRICE;
    }
    return this.distance * this.NORMAL_PRICE;
  }

  private isOvernight() {
    return (
      this.date.getHours() > this.OVERNIGHT_START_HOUR ||
      this.date.getHours() < this.OVERNIGHT_END_HOUR
    );
  }

  private isSunday() {
    return this.date.getDay() === 0;
  }
}
