export class InvalidDateException extends Error {
  constructor(){
    super("Invalid date");
  }
}